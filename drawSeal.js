function circle(ctx, obj, tmpStep, x, y, radius, speed, rot) {
    ctx.stroke();
    ctx.closePath()
    ctx.beginPath()
    radius = radius * obj.radius
    ctx.arc(x,y, radius, Math.PI * 2 * rot, Math.PI * 2 * Math.min(tmpStep, speed) / speed + Math.PI * 2 * rot)
    tmpStep -= speed

    if ('subs' in obj) {
        for (var i = 0; i < obj.subs.length; i++) {
            switch(obj.subs[i].type) {
            case "circle":
                if (tmpStep > 0) {
                    tmpStep = circle(ctx, obj.subs[i], tmpStep, x, y, radius, speed, rot)
                }
                break
            case "polygon":
                if (tmpStep > 0) {
                    tmpStep = polygon(ctx, obj.subs[i], tmpStep, x, y, radius, speed, rot)
                }
                break
            case "around":
                if (tmpStep > 0) {
                    tmpStep = around(ctx, obj.subs[i], tmpStep, x, y, radius, speed, rot)
                }
                break
            }
        }
    }

    return tmpStep

}

function polygon(ctx, obj, tmpStep, x, y, radius, speed, rot) {
    var stepDiv = speed / obj.sides
    var startAngle = obj.angle
    var angle = startAngle

    switch(obj.relation) {
    case "inner-tangent":
        break
    case "outer-tangent":
        radius = radius / Math.cos(Math.PI / obj.sides)
        break
    }

    var lastX = x + Math.cos(startAngle) * radius
    var lastY = y + Math.sin(startAngle) * radius
    ctx.moveTo(lastX,lastY)
    for (var i = 0; i < obj.sides && tmpStep > 0; i++) {
        angle += 2 * Math.PI * obj.jump / obj.sides
        var newX = x + Math.cos(angle) * radius
        var newY = y + Math.sin(angle) * radius
        ctx.lineTo(lastX + (newX - lastX) * Math.min(tmpStep, stepDiv) / stepDiv, lastY + (newY - lastY) * Math.min(tmpStep, stepDiv) / stepDiv)
        tmpStep -= stepDiv
        lastX = newX
        lastY = newY
    }

    if ('subs' in obj) {
        for (var i = 0; i < obj.subs.length; i++) {
            switch(obj.subs[i].type) {
            case "circle":
                if (tmpStep > 0) {
                    tmpStep = circle(ctx, obj.subs[i], tmpStep, x, y, radius, speed, rot)
                }
                break
            case "polygon":
                if (tmpStep > 0) {
                    tmpStep = polygon(ctx, obj.subs[i], tmpStep, x, y, radius, speed, rot)
                }
                break
            }
        }
    }


    return tmpStep
}

function around(ctx, obj, tmpStep, x, y, radius, speed, rot) {
    var stepDiv = speed / obj.count
    var startAngle = obj.angle
    var angle = startAngle
    for (var i = 0; i < obj.count && tmpStep > 0; i++) {
        angle += 2 * Math.PI / obj.count
        var newX = x + Math.cos(angle) * radius * obj.radius
        var newY = y + Math.sin(angle) * radius * obj.radius
        switch(obj.sub.type) {
        case "circle":
            tmpStep = circle(ctx, obj.sub, tmpStep, newX, newY, radius * obj.ratio, stepDiv, rot)
            break
        case "polygon":
            tmpStep = polygon(ctx, obj.sub, tmpStep, newX, newY, radius * obj.ratio, stepDiv, rot)
            break
        }
    }
    return tmpStep
}
