import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    visible: true
    width: 800
    height: 800

    title: qsTr("Hello World")

    property bool shadow: true
    property var allSeal: []
    property int gridSize: 1

    Item {
        anchors.fill: parent
        focus: true
        Keys.onPressed: {
            if (event.key === Qt.Key_Space) {
                for (var i = 0; i < allSeal.length; i++) {
                    allSeal[i].regenerate()
                }
            } else if (event.key === Qt.Key_T) {
                shadow = !shadow
                for (var i = 0; i < allSeal.length; i++) {
                    allSeal[i].repaint()
                }
            } else if (event.key === Qt.Key_P) {
                gridSize++
            } else if (event.key === Qt.Key_O) {
                gridSize--
            } else if (event.key >= Qt.Key_0 && event.key <= Qt.Key_9 ) {
                gridSize = event.key - Qt.Key_0
            }
        }
    }

    /*Experiment {
        anchors.centerIn: parent
        width: Math.min(parent.width, parent.height)
        height: width
    }*/

    Grid {
        id: grid
        columns: gridSize
        rows: gridSize
        anchors.centerIn: parent
        width: columns * Math.min(parent.width / columns, parent.height / rows)
        height: rows * Math.min(parent.width / columns, parent.height / rows)

        Repeater {
            model:grid.columns * grid.rows
            delegate: SealDrawer {
                id: drawer
                width: parent.width / grid.columns
                height: parent.height / grid.rows

                Component.onCompleted: {
                    allSeal[index] = drawer
                }
            }
        }
    }


}
