import QtQuick 2.0

Item {
    Canvas {
          id: mycanvas
          anchors.centerIn: parent
          width: Math.min(parent.width, parent.height)-10
          height: width
          onPaint: {
              var ctx = getContext("2d")
              var center = width * 0.5
              ctx.fillStyle = Qt.rgba(0, 0, 0, 1)
              ctx.fillRect(0, 0, width, height)
              var radius = width * 0.2


              ctx.lineWidth = 2
              ctx.fillStyle = Qt.rgba(1, 1, 1, 1);
              ctx.strokeStyle = Qt.rgba(1, 1, 1, 1);

              ctx.beginPath()

              ctx.strokeStyle = Qt.rgba(1, 1, 1, 0.03);
              for (var i = 10; i > 2; i -= 0.8) {
                  ctx.lineWidth = 10
                  ctx.arc(center, center, radius, 0, Math.PI * 2)
                  ctx.stroke()
              }

              ctx.lineWidth = 2
              ctx.strokeStyle = Qt.rgba(1, 1, 1, 1);
              ctx.arc(center, center, radius, 0, Math.PI * 2)
              ctx.stroke()

              ctx.closePath()
          }
      }
}
