function generateRandomSeal() {
    timer.stop()
    done = false
    var tmpseal = [makeRandomPolygon(0), makeRandomCircle(0)][Math.floor(Math.random() * 2)]
    seal = tmpseal

    if (animate) {
        step = 0
        timer.start()
    } else {
        step = 9999
        mycanvas.requestPaint()
    }
}

function makeRandomPolygon(level) {
    var obj = {}
    obj.type = "polygon"
    obj.relation = ["outer-tangent", "inner-tangent"][Math.floor(Math.random() * 2)]
    obj.sides = Math.floor(Math.random() * 6) + 3
    do {
        var flag = false
        obj.jump = Math.floor(Math.random() * Math.floor(obj.sides)) + 1
        for (var i = 2; i <= obj.sides; i++) {
            if (obj.sides % i == 0 && obj.jump % i == 0) {
                flag = true
                break
            }
        }
    } while (flag)
    if (obj.sides % 2 == 0) {
        obj.angle = [0, Math.PI / obj.sides][Math.floor(Math.random() * 2)]
    } else {
        obj.angle = [Math.PI / 2, -Math.PI / 2][Math.floor(Math.random() * 2)]
    }

    obj.subs = []

    if (level < maxComplexity || Math.floor(Math.random() * level) < maxComplexity - 1) {
        var subSelect = Math.floor(Math.random() * 4)
        if (subSelect | 1) {
            obj.subs.push(makeRandomAround(level+1))
        }
        if (subSelect | 2) {
            obj.subs.push(makeRandomCircle(level+1))
        }
    }

    return obj
}

function makeRandomCircle(level) {
    var obj = {}
    obj.type = "circle"
    obj.radius = 0.6 + Math.random() / 2

    obj.subs = []

    if (level < maxComplexity || Math.floor(Math.random() * level) < maxComplexity - 1) {
        var subSelect = Math.floor(Math.random() * 4)
        if (subSelect | 1) {
            obj.subs.push(makeRandomAround(level+1))
        }
        if (subSelect | 2) {
            obj.subs.push(makeRandomPolygon(level+1))
        }
    }

    return obj
}

function makeRandomAround(level) {
    var obj = {}
    obj.type = "around"
    obj.count = Math.floor(Math.random() * 6) + 3
    obj.angle =  [Math.PI / 2, -Math.PI / 2][Math.floor(Math.random() * 2)]
    obj.ratio = 0.1 + Math.random() / 4
    obj.radius = 0.75 + Math.random() / 2
    obj.sub = Math.random() > 0.8 ? makeRandomPolygon(level+1) : makeRandomCircle(level+1)
    return obj
}

function makeRandomBehind(level) {
    var obj = {}
    obj.type = "behind"
    obj.count = Math.floor(Math.random() * 6) + 3
    obj.angle =  [Math.PI / 2, -Math.PI / 2][Math.floor(Math.random() * 2)]
    obj.radius = 0.75 + Math.random() / 2
    obj.sub = Math.random() > 0.5 ? makeRandomPolygon(level+1) : makeRandomCircle(level+1)
    return obj
}
