import QtQuick 2.0
import "generateSeal.js" as Seal
import "drawSeal.js" as Draw

Item {
    id: sealItem
    property bool animate: true
    property var step: 0
    property var speed: 20
    property bool done: false
    property int maxComplexity: 2
    property var seal

    signal regenerate
    signal repaint


    /*Timer {
        id: rotTimer
        interval: 40
        running: done
        repeat: true
        onTriggered: {
            mycanvas.rotation++
        }
    }*/


    onRegenerate: {
        Seal.generateRandomSeal()
    }

    onRepaint: {
        mycanvas.requestPaint()
    }

    Component.onCompleted: regenerate()

    Timer {
        id: timer
        interval: 40
        running: false
        repeat: !done
        onTriggered: {
            step++
            mycanvas.requestPaint()
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
    }

    Canvas {
          id: mycanvas
          transformOrigin: Item.Center
          anchors.centerIn: parent
          width: Math.min(parent.width, parent.height)-10
          height: width
          onPaint: {
              var tmpStep = step
              var ctx = getContext("2d");
              var center = width * 0.5
              ctx.fillStyle = Qt.rgba(0, 0, 0, 1);
              ctx.fillRect(0, 0, width, height);
              var radius = width * 0.2
              if (done && shadow) {
                  ctx.shadowColor = Qt.rgba(Math.random(),Math.random(),Math.random(),1);
                  ctx.shadowBlur = 10;
                  //var scale = 1 + (20 - step % 40) / 10000
                  //ctx.scale(scale, scale)
              } else {
                  ctx.shadowColor = 'transparent';
              }

              ctx.lineWidth = 2
              ctx.fillStyle = Qt.rgba(1, 1, 1, 1);
              ctx.strokeStyle = Qt.rgba(1, 1, 1, 1);
              ctx.beginPath()

              switch(seal.type) {
              case "circle":
                  if (tmpStep > 0) {
                      tmpStep = Draw.circle(ctx, seal, tmpStep, center, center, radius, speed, 0)
                  }
                  tmpStep -= speed

                  break
              case "polygon":
                  if (tmpStep > 0) {
                      tmpStep = Draw.polygon(ctx, seal, tmpStep, center, center, radius, speed, 0)
                  }
                  tmpStep -= speed
                  break
              }

              ctx.stroke();
              ctx.closePath()

              if (tmpStep > 0) {
                  done = true
              }
              ctx.setTransform(1, 0, 0, 1, 0, 0);
          }
      }



    /*Text {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        font.pixelSize: 15
        text: "done" + (shadow ? " with shadow" : "")
        color: "white"
        visible: done
    }*/
}
